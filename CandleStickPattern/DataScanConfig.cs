﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace CandleStickPattern
{
    public partial class DataScanConfig : Form
    {
        public IEnumerable<string> Folders
        {
            get
            {
                return textBox_RootFolder.Text.Split(',');
            }
        }

        public TradeConfig TradeConfig
        {
            get
            {
                return new TradeConfig
                {
                    Fee = (double)numericUpDown_Fee.Value,
                    Threshold = (double)numericUpDown_ProfitThreshold.Value,
                    GreedMode = checkBox_GreedMode.Checked,
                    SingleContract = checkBox_SingleContract.Checked
                };
            }
        }

        public string SelectedPath
        {
            get
            {
                return textBox_RootFolder.Text;
            }
        }

        public DataScanConfig()
        {
            InitializeComponent();

            var tradeConfig = ConfigHelper.GetTradeConfig();
            if (tradeConfig != null)
            {
                numericUpDown_Fee.Value = (decimal)tradeConfig.Fee;
                numericUpDown_ProfitThreshold.Value = (decimal)tradeConfig.Threshold;
                checkBox_GreedMode.Checked = tradeConfig.GreedMode;
                checkBox_SingleContract.Checked = tradeConfig.SingleContract;
            }

            var initDirectory = ConfigHelper.GetLastDirectory();
            if (string.IsNullOrEmpty(initDirectory) == false)
            {
                textBox_RootFolder.Text = initDirectory;
            }
        }


        private void Button_Browse_Click(object sender, EventArgs e)
        {
            var folderDialog = new FolderSelectDialog();
            folderDialog.Multiselect = true;

            if (string.IsNullOrEmpty(ConfigHelper.GetLastDirectory()) == false)
            {
                folderDialog.InitialDirectory = Path.GetDirectoryName(Folders.First());
            }

            if (folderDialog.ShowDialog())
            {
                textBox_RootFolder.SelectedText = string.Join(", ",folderDialog.FileNames);
            }
        }

        private void Button_Scan_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            ConfigHelper.SetTradeConfig(TradeConfig);
        }
    }
}
