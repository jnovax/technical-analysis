﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CandleStickPattern
{
    public class Order
    {
        public const int MAX_GREED = 3;
        public const double PROFIT_LOSS_BALANCE = 0.75;

        public const string POSITION_LONG = "LONG";
        public const string POSITION_SHORT = "SHORT";

        public int Id { get; set; }
        public bool Greed { get; set; }
        public int GreedCounter { get; set; }
        public string Position { get; set; }
        public double Price { get; set; }
        public double Threshold { get; set; }
        public double TakeProfit { get; private set; }
        public double StopLoss { get; private set; }
        public bool IsDone { get; private set; }
        public double SettlePrice { get; set; }

        public double Profit(double fee)
        {
            if (IsDone)
            {
                if (Position == POSITION_LONG)
                {
                    return (SettlePrice - Price) * 100000 - fee;
                }
                else
                {
                    return (Price - SettlePrice) * 100000 - fee;
                }
            }
            else
            {
                return 0d;
            }
        }

        public Order(int id, double price, double threshold, string pos, bool greedMod)
        {
            Id = id;
            Position = pos;
            Price = price;
            Threshold = threshold;
            Greed = greedMod;

            ResetGreed(price);
        }

        public void ResetGreed(double price)
        {
            if (Greed)
            {
                GreedCounter = MAX_GREED;
            }

            if (Position == POSITION_LONG)
            {
                TakeProfit = price + Threshold;
                StopLoss = price - (Threshold * PROFIT_LOSS_BALANCE);
            }
            else
            {
                TakeProfit = price - Threshold;
                StopLoss = price + (Threshold * PROFIT_LOSS_BALANCE);
            }
        }

        public string PerformStep(double price)
        {
            if (Position == POSITION_LONG)
            {
                if (price >= TakeProfit)
                {
                    if (GreedCounter > 1)
                    {
                        GreedCounter--;

                        var newThreshold = Threshold * GreedCounter / MAX_GREED;
                        TakeProfit = price + newThreshold;
                        StopLoss = price - (newThreshold * PROFIT_LOSS_BALANCE);

                        return $"GREED@{TakeProfit.ToString("N2")}";
                    }
                    else
                    {
                        return Settle(price);
                    }
                }
                else if (price <= StopLoss)
                {
                    return Settle(price);
                }
            }
            else
            {
                if (price <= TakeProfit)
                {
                    if (GreedCounter > 1)
                    {
                        GreedCounter--;

                        var newThreshold = Threshold * GreedCounter / MAX_GREED;
                        TakeProfit = price - newThreshold;
                        StopLoss = price + (newThreshold * PROFIT_LOSS_BALANCE);

                        return $"GREED@{TakeProfit.ToString("N2")}";
                    }
                    else
                    {
                        return Settle(price);
                    }
                }
                else if (price >= StopLoss)
                {
                    return Settle(price);
                }
            }
            return String.Empty;
        }

        public string Settle(double price)
        {
            SettlePrice = price;
            IsDone = true;

            return "SETTLE";
        }
    }
}
