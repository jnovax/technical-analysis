﻿using System;
using System.Windows.Forms;

namespace CandleStickPattern
{
    public partial class AutoTradeConfig : Form
    {
        public AutoTradeConfig()
        {
            InitializeComponent();

            var tradeConfig = ConfigHelper.GetTradeConfig();
            if (tradeConfig != null)
            {
                numericUpDown_Fee.Value = (decimal)tradeConfig.Fee;
                numericUpDown_ProfitThreshold.Value = (decimal)tradeConfig.Threshold;
                checkBox_GreedMode.Checked = tradeConfig.GreedMode;
                checkBox_SingleContract.Checked = tradeConfig.SingleContract;
            }
        }

        private void Button_Trade_Click(object sender, EventArgs e)
        {
            var tradeConfig = new TradeConfig
            {
                Fee = (double)numericUpDown_Fee.Value,
                Threshold = (double)numericUpDown_ProfitThreshold.Value,
                GreedMode = checkBox_GreedMode.Checked,
                SingleContract = checkBox_SingleContract.Checked
            };
            ConfigHelper.SetTradeConfig(tradeConfig);

            FormMain.TheMainWnd.AutoTrade(tradeConfig);
        }
    }
}
