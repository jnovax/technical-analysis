﻿namespace CandleStickPattern
{
    public class TradeConfig
    {
        public double Fee { get; set; }
        public double Threshold { get; set; }
        public bool GreedMode { get; set; }
        public bool SingleContract { get; set; }
    }
}
