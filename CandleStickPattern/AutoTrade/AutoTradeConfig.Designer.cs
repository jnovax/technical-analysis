﻿namespace CandleStickPattern
{
    partial class AutoTradeConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.numericUpDown_Fee = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_ProfitThreshold = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.checkBox_GreedMode = new System.Windows.Forms.CheckBox();
            this.button_Trade = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.checkBox_SingleContract = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Fee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ProfitThreshold)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(78, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Fee";
            // 
            // numericUpDown_Fee
            // 
            this.numericUpDown_Fee.Increment = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDown_Fee.Location = new System.Drawing.Point(116, 54);
            this.numericUpDown_Fee.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDown_Fee.Name = "numericUpDown_Fee";
            this.numericUpDown_Fee.Size = new System.Drawing.Size(108, 22);
            this.numericUpDown_Fee.TabIndex = 1;
            this.numericUpDown_Fee.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numericUpDown_Fee.ThousandsSeparator = true;
            this.numericUpDown_Fee.Value = new decimal(new int[] {
            9000,
            0,
            0,
            0});
            // 
            // numericUpDown_ProfitThreshold
            // 
            this.numericUpDown_ProfitThreshold.DecimalPlaces = 1;
            this.numericUpDown_ProfitThreshold.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDown_ProfitThreshold.Location = new System.Drawing.Point(116, 12);
            this.numericUpDown_ProfitThreshold.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDown_ProfitThreshold.Name = "numericUpDown_ProfitThreshold";
            this.numericUpDown_ProfitThreshold.Size = new System.Drawing.Size(108, 22);
            this.numericUpDown_ProfitThreshold.TabIndex = 1;
            this.numericUpDown_ProfitThreshold.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numericUpDown_ProfitThreshold.Value = new decimal(new int[] {
            15,
            0,
            0,
            65536});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(33, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Take Profit";
            // 
            // checkBox_GreedMode
            // 
            this.checkBox_GreedMode.AutoSize = true;
            this.checkBox_GreedMode.Checked = true;
            this.checkBox_GreedMode.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_GreedMode.Location = new System.Drawing.Point(259, 55);
            this.checkBox_GreedMode.Name = "checkBox_GreedMode";
            this.checkBox_GreedMode.Size = new System.Drawing.Size(109, 21);
            this.checkBox_GreedMode.TabIndex = 3;
            this.checkBox_GreedMode.Text = "Greed Mode";
            this.checkBox_GreedMode.UseVisualStyleBackColor = true;
            // 
            // button_Trade
            // 
            this.button_Trade.Location = new System.Drawing.Point(159, 129);
            this.button_Trade.Name = "button_Trade";
            this.button_Trade.Size = new System.Drawing.Size(138, 42);
            this.button_Trade.TabIndex = 4;
            this.button_Trade.Text = "Trade";
            this.button_Trade.UseVisualStyleBackColor = true;
            this.button_Trade.Click += new System.EventHandler(this.Button_Trade_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(256, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(198, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = "Stop Loss is 2/3 of Take Profit";
            // 
            // checkBox_SingleContract
            // 
            this.checkBox_SingleContract.AutoSize = true;
            this.checkBox_SingleContract.Location = new System.Drawing.Point(259, 93);
            this.checkBox_SingleContract.Name = "checkBox_SingleContract";
            this.checkBox_SingleContract.Size = new System.Drawing.Size(165, 21);
            this.checkBox_SingleContract.TabIndex = 6;
            this.checkBox_SingleContract.Text = "Single Contract Mode";
            this.checkBox_SingleContract.UseVisualStyleBackColor = true;
            // 
            // AutoTradeConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 183);
            this.Controls.Add(this.checkBox_SingleContract);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button_Trade);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.checkBox_GreedMode);
            this.Controls.Add(this.numericUpDown_ProfitThreshold);
            this.Controls.Add(this.numericUpDown_Fee);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AutoTradeConfig";
            this.Text = "Auto Trade";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Fee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ProfitThreshold)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numericUpDown_Fee;
        private System.Windows.Forms.NumericUpDown numericUpDown_ProfitThreshold;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox checkBox_GreedMode;
        private System.Windows.Forms.Button button_Trade;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox checkBox_SingleContract;
    }
}