﻿using System;

namespace CandleStickPattern
{
    public struct HLOC
    {
        public DateTime Time { get; set; }
        private double high;
        private double low;
        private double open;
        private double close;

        public double High { get => high; set => high = Math.Round(value, 1); }
        public double Low { get => low; set => low = Math.Round(value, 1); }
        public double Open { get => open; set => open = Math.Round(value, 1); }
        public double Close { get => close; set => close = Math.Round(value, 1); }

        public object[] GetValues()
        {
            return new object[] { high, low, open, close };
        }
    }
}
