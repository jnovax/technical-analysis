﻿using System;

namespace CandleStickPattern
{
    public class OrderDeal
    {
        public string sc { get; set; }
        public string type { get; set; }
        public string time { get; set; }
        public OrderDealContext data { get; set; }
        public string raw_data { get; set; }
    }

    public class OrderDealContext
    {
        public string cmd { get; set; }
        public string oID { get; set; }
        public int rc { get; set; }
        public string rs { get; set; }
        public OrderDealData data { get; set; }
    }

    [Serializable]
    public class OrderDealData
    {
        public long tick { get; set; }
        public string symbol { get; set; }
        public object centerNo { get; set; }
        public string channel { get; set; }
        public string mkid { get; set; }
        public int uLevel { get; set; }
        public string type { get; set; }
        public string branch { get; set; }
        public string origin_accCode { get; set; }
        public string rslist { get; set; }
        public string orderTime { get; set; }
        public string quote { get; set; }
        public string subbranch { get; set; }
        public string accType { get; set; }
        public int matchVolume { get; set; }
        public string group { get; set; }
        public string accountCode { get; set; }
        public string side { get; set; }
        public int orderNo { get; set; }
        public string authorised_accCode { get; set; }
        public string permission { get; set; }
        public int volume { get; set; }
        public string market { get; set; }
        public string pk_orderNo { get; set; }
        public string showPrice { get; set; }
        public string msg_type { get; set; }
        public string status { get; set; }
        public string shareStatus { get; set; }
        public long matchValue { get; set; }
        public string oType { get; set; }
    }

}
