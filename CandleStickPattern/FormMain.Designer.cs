﻿namespace CandleStickPattern
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.chart_CandleStick = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel_ControlBar = new System.Windows.Forms.Panel();
            this.button_AutoTrade = new System.Windows.Forms.Button();
            this.button_IndicatorConfig = new System.Windows.Forms.Button();
            this.numericUpDown_TimeFrame = new System.Windows.Forms.NumericUpDown();
            this.button_ScanDataFolder = new System.Windows.Forms.Button();
            this.button_ImportData = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.label_X_Axis = new System.Windows.Forms.Label();
            this.label_Y_Axis = new System.Windows.Forms.Label();
            this.button_ToggleFullscreen = new System.Windows.Forms.Label();
            this.button_ShowHideLog = new System.Windows.Forms.Label();
            this.richTextBox_Log = new System.Windows.Forms.RichTextBox();
            this.backgroundWorkerImportOneDay = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorkerScanFolder = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.chart_CandleStick)).BeginInit();
            this.panel_ControlBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_TimeFrame)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // chart_CandleStick
            // 
            this.chart_CandleStick.BackColor = System.Drawing.Color.Black;
            chartArea1.AxisX.LabelStyle.ForeColor = System.Drawing.Color.White;
            chartArea1.AxisX.LineColor = System.Drawing.Color.DarkGray;
            chartArea1.AxisY.LabelStyle.ForeColor = System.Drawing.Color.White;
            chartArea1.AxisY.LineColor = System.Drawing.Color.DarkGray;
            chartArea1.AxisY.ScaleBreakStyle.StartFromZero = System.Windows.Forms.DataVisualization.Charting.StartFromZero.No;
            chartArea1.AxisY2.LabelStyle.ForeColor = System.Drawing.Color.White;
            chartArea1.AxisY2.LineColor = System.Drawing.Color.White;
            chartArea1.BackColor = System.Drawing.Color.Black;
            chartArea1.BorderColor = System.Drawing.Color.Silver;
            chartArea1.CursorX.IsUserSelectionEnabled = true;
            chartArea1.CursorX.LineColor = System.Drawing.Color.Gold;
            chartArea1.CursorX.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dash;
            chartArea1.CursorY.Interval = 0.1D;
            chartArea1.CursorY.IsUserSelectionEnabled = true;
            chartArea1.CursorY.LineColor = System.Drawing.Color.Gold;
            chartArea1.CursorY.LineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dash;
            chartArea1.Name = "PriceChartArea";
            chartArea1.Position.Auto = false;
            chartArea1.Position.Height = 100F;
            chartArea1.Position.Width = 100F;
            this.chart_CandleStick.ChartAreas.Add(chartArea1);
            this.chart_CandleStick.Cursor = System.Windows.Forms.Cursors.Cross;
            this.chart_CandleStick.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chart_CandleStick.Location = new System.Drawing.Point(0, 0);
            this.chart_CandleStick.Name = "chart_CandleStick";
            series1.ChartArea = "PriceChartArea";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Candlestick;
            series1.CustomProperties = "PriceDownColor=Brown, PointWidth=0.5, PriceUpColor=LimeGreen";
            series1.LabelForeColor = System.Drawing.Color.White;
            series1.MarkerColor = System.Drawing.Color.White;
            series1.MarkerSize = 10;
            series1.Name = "DealsHLOC";
            series1.YValuesPerPoint = 4;
            this.chart_CandleStick.Series.Add(series1);
            this.chart_CandleStick.Size = new System.Drawing.Size(1205, 674);
            this.chart_CandleStick.TabIndex = 0;
            this.chart_CandleStick.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Chart_CandleStick_MouseMove);
            // 
            // panel_ControlBar
            // 
            this.panel_ControlBar.Controls.Add(this.button_AutoTrade);
            this.panel_ControlBar.Controls.Add(this.button_IndicatorConfig);
            this.panel_ControlBar.Controls.Add(this.numericUpDown_TimeFrame);
            this.panel_ControlBar.Controls.Add(this.button_ScanDataFolder);
            this.panel_ControlBar.Controls.Add(this.button_ImportData);
            this.panel_ControlBar.Controls.Add(this.label1);
            this.panel_ControlBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_ControlBar.Location = new System.Drawing.Point(0, 0);
            this.panel_ControlBar.Name = "panel_ControlBar";
            this.panel_ControlBar.Size = new System.Drawing.Size(1205, 70);
            this.panel_ControlBar.TabIndex = 1;
            // 
            // button_AutoTrade
            // 
            this.button_AutoTrade.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_AutoTrade.Location = new System.Drawing.Point(499, 9);
            this.button_AutoTrade.Name = "button_AutoTrade";
            this.button_AutoTrade.Size = new System.Drawing.Size(137, 48);
            this.button_AutoTrade.TabIndex = 7;
            this.button_AutoTrade.Text = "Auto Trade";
            this.button_AutoTrade.UseVisualStyleBackColor = true;
            this.button_AutoTrade.Click += new System.EventHandler(this.Button_AutoTrade_Click);
            // 
            // button_IndicatorConfig
            // 
            this.button_IndicatorConfig.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_IndicatorConfig.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_IndicatorConfig.Location = new System.Drawing.Point(1130, 10);
            this.button_IndicatorConfig.Name = "button_IndicatorConfig";
            this.button_IndicatorConfig.Size = new System.Drawing.Size(63, 47);
            this.button_IndicatorConfig.TabIndex = 6;
            this.button_IndicatorConfig.Text = "⬙";
            this.button_IndicatorConfig.UseVisualStyleBackColor = true;
            this.button_IndicatorConfig.Click += new System.EventHandler(this.Button_IndicatorConfig_Click);
            // 
            // numericUpDown_TimeFrame
            // 
            this.numericUpDown_TimeFrame.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDown_TimeFrame.Location = new System.Drawing.Point(148, 18);
            this.numericUpDown_TimeFrame.Maximum = new decimal(new int[] {
            600,
            0,
            0,
            0});
            this.numericUpDown_TimeFrame.Name = "numericUpDown_TimeFrame";
            this.numericUpDown_TimeFrame.Size = new System.Drawing.Size(58, 30);
            this.numericUpDown_TimeFrame.TabIndex = 4;
            this.numericUpDown_TimeFrame.ValueChanged += new System.EventHandler(this.NumericUpDown_TimeFrame_ValueChanged);
            // 
            // button_ScanDataFolder
            // 
            this.button_ScanDataFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_ScanDataFolder.Location = new System.Drawing.Point(827, 10);
            this.button_ScanDataFolder.Name = "button_ScanDataFolder";
            this.button_ScanDataFolder.Size = new System.Drawing.Size(297, 47);
            this.button_ScanDataFolder.TabIndex = 2;
            this.button_ScanDataFolder.Text = "Scan Data Folder with All Patterns";
            this.button_ScanDataFolder.UseVisualStyleBackColor = true;
            this.button_ScanDataFolder.Click += new System.EventHandler(this.Button_ScanDataFolder_Click);
            // 
            // button_ImportData
            // 
            this.button_ImportData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_ImportData.Location = new System.Drawing.Point(642, 10);
            this.button_ImportData.Name = "button_ImportData";
            this.button_ImportData.Size = new System.Drawing.Size(179, 47);
            this.button_ImportData.TabIndex = 0;
            this.button_ImportData.Text = "Import 1-Day Data";
            this.button_ImportData.UseVisualStyleBackColor = true;
            this.button_ImportData.Click += new System.EventHandler(this.Button_ImportData_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(25, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(211, 25);
            this.label1.TabIndex = 5;
            this.label1.Text = "Time Frame              (s)";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 70);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.label_X_Axis);
            this.splitContainer1.Panel1.Controls.Add(this.label_Y_Axis);
            this.splitContainer1.Panel1.Controls.Add(this.button_ToggleFullscreen);
            this.splitContainer1.Panel1.Controls.Add(this.button_ShowHideLog);
            this.splitContainer1.Panel1.Controls.Add(this.chart_CandleStick);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.richTextBox_Log);
            this.splitContainer1.Panel2Collapsed = true;
            this.splitContainer1.Size = new System.Drawing.Size(1205, 674);
            this.splitContainer1.SplitterDistance = 477;
            this.splitContainer1.SplitterWidth = 8;
            this.splitContainer1.TabIndex = 3;
            // 
            // label_X_Axis
            // 
            this.label_X_Axis.AutoSize = true;
            this.label_X_Axis.BackColor = System.Drawing.Color.Black;
            this.label_X_Axis.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_X_Axis.ForeColor = System.Drawing.Color.White;
            this.label_X_Axis.Location = new System.Drawing.Point(575, 633);
            this.label_X_Axis.Name = "label_X_Axis";
            this.label_X_Axis.Size = new System.Drawing.Size(0, 20);
            this.label_X_Axis.TabIndex = 5;
            // 
            // label_Y_Axis
            // 
            this.label_Y_Axis.AutoSize = true;
            this.label_Y_Axis.BackColor = System.Drawing.Color.Black;
            this.label_Y_Axis.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Y_Axis.ForeColor = System.Drawing.Color.White;
            this.label_Y_Axis.Location = new System.Drawing.Point(1130, 162);
            this.label_Y_Axis.Name = "label_Y_Axis";
            this.label_Y_Axis.Size = new System.Drawing.Size(0, 20);
            this.label_Y_Axis.TabIndex = 4;
            // 
            // button_ToggleFullscreen
            // 
            this.button_ToggleFullscreen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_ToggleFullscreen.BackColor = System.Drawing.Color.Black;
            this.button_ToggleFullscreen.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_ToggleFullscreen.ForeColor = System.Drawing.Color.White;
            this.button_ToggleFullscreen.Location = new System.Drawing.Point(1173, -2);
            this.button_ToggleFullscreen.Name = "button_ToggleFullscreen";
            this.button_ToggleFullscreen.Size = new System.Drawing.Size(32, 32);
            this.button_ToggleFullscreen.TabIndex = 3;
            this.button_ToggleFullscreen.Text = "▣";
            this.button_ToggleFullscreen.Click += new System.EventHandler(this.Button_ToggleFullscreen_Click);
            // 
            // button_ShowHideLog
            // 
            this.button_ShowHideLog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button_ShowHideLog.BackColor = System.Drawing.Color.Black;
            this.button_ShowHideLog.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_ShowHideLog.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.button_ShowHideLog.Location = new System.Drawing.Point(3, 633);
            this.button_ShowHideLog.Name = "button_ShowHideLog";
            this.button_ShowHideLog.Size = new System.Drawing.Size(32, 32);
            this.button_ShowHideLog.TabIndex = 2;
            this.button_ShowHideLog.Tag = "HIDE";
            this.button_ShowHideLog.Text = "◥";
            this.button_ShowHideLog.Click += new System.EventHandler(this.Button_ShowHideLog_Click);
            // 
            // richTextBox_Log
            // 
            this.richTextBox_Log.BackColor = System.Drawing.Color.Black;
            this.richTextBox_Log.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox_Log.ForeColor = System.Drawing.Color.White;
            this.richTextBox_Log.Location = new System.Drawing.Point(0, 0);
            this.richTextBox_Log.Name = "richTextBox_Log";
            this.richTextBox_Log.ReadOnly = true;
            this.richTextBox_Log.Size = new System.Drawing.Size(150, 46);
            this.richTextBox_Log.TabIndex = 3;
            this.richTextBox_Log.Text = "";
            // 
            // backgroundWorkerImportOneDay
            // 
            this.backgroundWorkerImportOneDay.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BackgroundWorkerImportOneDay_DoWork);
            this.backgroundWorkerImportOneDay.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.BackgroundWorkerImportOneDay_RunWorkerCompleted);
            // 
            // backgroundWorkerScanFolder
            // 
            this.backgroundWorkerScanFolder.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BackgroundWorkerScanFolder_DoWork);
            this.backgroundWorkerScanFolder.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.BackgroundWorkerScanFolder_RunWorkerCompleted);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1205, 744);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.panel_ControlBar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormMain";
            this.Text = "Candlestick Pattern Test";
            ((System.ComponentModel.ISupportInitialize)(this.chart_CandleStick)).EndInit();
            this.panel_ControlBar.ResumeLayout(false);
            this.panel_ControlBar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_TimeFrame)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart chart_CandleStick;
        private System.Windows.Forms.Panel panel_ControlBar;
        private System.Windows.Forms.Button button_ImportData;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.RichTextBox richTextBox_Log;
        private System.ComponentModel.BackgroundWorker backgroundWorkerImportOneDay;
        private System.Windows.Forms.Button button_ScanDataFolder;
        private System.ComponentModel.BackgroundWorker backgroundWorkerScanFolder;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numericUpDown_TimeFrame;
        private System.Windows.Forms.Button button_IndicatorConfig;
        private System.Windows.Forms.Label button_ShowHideLog;
        private System.Windows.Forms.Label button_ToggleFullscreen;
        private System.Windows.Forms.Button button_AutoTrade;
        private System.Windows.Forms.Label label_X_Axis;
        private System.Windows.Forms.Label label_Y_Axis;
    }
}

