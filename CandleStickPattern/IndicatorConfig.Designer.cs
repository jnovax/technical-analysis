﻿namespace CandleStickPattern
{
    partial class IndicatorConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.button_SelectNoPattern = new System.Windows.Forms.Button();
            this.button_SelectAllShort = new System.Windows.Forms.Button();
            this.button_AllPatterns = new System.Windows.Forms.Button();
            this.button_SelectAllLong = new System.Windows.Forms.Button();
            this.checkedListBox_Indicators = new System.Windows.Forms.CheckedListBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.button_Hide = new System.Windows.Forms.Button();
            this.propertyGrid_Indicator = new System.Windows.Forms.PropertyGrid();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Controls.Add(this.button_SelectNoPattern, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.button_SelectAllShort, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.button_AllPatterns, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.button_SelectAllLong, 3, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(282, 50);
            this.tableLayoutPanel1.TabIndex = 4;
            // 
            // button_SelectNoPattern
            // 
            this.button_SelectNoPattern.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_SelectNoPattern.Location = new System.Drawing.Point(143, 3);
            this.button_SelectNoPattern.Name = "button_SelectNoPattern";
            this.button_SelectNoPattern.Size = new System.Drawing.Size(64, 44);
            this.button_SelectNoPattern.TabIndex = 4;
            this.button_SelectNoPattern.Text = "None";
            this.button_SelectNoPattern.UseVisualStyleBackColor = true;
            this.button_SelectNoPattern.Click += new System.EventHandler(this.Button_SelectNoPattern_Click);
            // 
            // button_SelectAllShort
            // 
            this.button_SelectAllShort.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_SelectAllShort.Location = new System.Drawing.Point(3, 3);
            this.button_SelectAllShort.Name = "button_SelectAllShort";
            this.button_SelectAllShort.Size = new System.Drawing.Size(64, 44);
            this.button_SelectAllShort.TabIndex = 0;
            this.button_SelectAllShort.Text = "Short (▼)";
            this.button_SelectAllShort.UseVisualStyleBackColor = true;
            this.button_SelectAllShort.Click += new System.EventHandler(this.Button_SelectAllShort_Click);
            // 
            // button_AllPatterns
            // 
            this.button_AllPatterns.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_AllPatterns.Location = new System.Drawing.Point(73, 3);
            this.button_AllPatterns.Name = "button_AllPatterns";
            this.button_AllPatterns.Size = new System.Drawing.Size(64, 44);
            this.button_AllPatterns.TabIndex = 2;
            this.button_AllPatterns.Text = "All";
            this.button_AllPatterns.UseVisualStyleBackColor = true;
            this.button_AllPatterns.Click += new System.EventHandler(this.Button_AllPatterns_Click);
            // 
            // button_SelectAllLong
            // 
            this.button_SelectAllLong.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_SelectAllLong.Location = new System.Drawing.Point(213, 3);
            this.button_SelectAllLong.Name = "button_SelectAllLong";
            this.button_SelectAllLong.Size = new System.Drawing.Size(66, 44);
            this.button_SelectAllLong.TabIndex = 1;
            this.button_SelectAllLong.Text = "Long (▲)";
            this.button_SelectAllLong.UseVisualStyleBackColor = true;
            this.button_SelectAllLong.Click += new System.EventHandler(this.Button_SelectAllLong_Click);
            // 
            // checkedListBox_Indicators
            // 
            this.checkedListBox_Indicators.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkedListBox_Indicators.FormattingEnabled = true;
            this.checkedListBox_Indicators.Location = new System.Drawing.Point(0, 0);
            this.checkedListBox_Indicators.Name = "checkedListBox_Indicators";
            this.checkedListBox_Indicators.Size = new System.Drawing.Size(282, 228);
            this.checkedListBox_Indicators.TabIndex = 5;
            this.checkedListBox_Indicators.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.CheckedListBox_Patterns_ItemCheck);
            this.checkedListBox_Indicators.MouseClick += new System.Windows.Forms.MouseEventHandler(this.CheckedListBox_Indicators_MouseClick);
            this.checkedListBox_Indicators.MouseUp += new System.Windows.Forms.MouseEventHandler(this.CheckedListBox_Indicators_MouseUp);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 50);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.checkedListBox_Indicators);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.button_Hide);
            this.splitContainer1.Panel2.Controls.Add(this.propertyGrid_Indicator);
            this.splitContainer1.Size = new System.Drawing.Size(282, 457);
            this.splitContainer1.SplitterDistance = 228;
            this.splitContainer1.TabIndex = 6;
            // 
            // button_Hide
            // 
            this.button_Hide.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Hide.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_Hide.Location = new System.Drawing.Point(250, 2);
            this.button_Hide.Name = "button_Hide";
            this.button_Hide.Size = new System.Drawing.Size(29, 27);
            this.button_Hide.TabIndex = 6;
            this.button_Hide.Text = "◥";
            this.button_Hide.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button_Hide.UseVisualStyleBackColor = true;
            this.button_Hide.Click += new System.EventHandler(this.Button_Hide_Click);
            // 
            // propertyGrid_Indicator
            // 
            this.propertyGrid_Indicator.CommandsVisibleIfAvailable = false;
            this.propertyGrid_Indicator.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propertyGrid_Indicator.HelpVisible = false;
            this.propertyGrid_Indicator.Location = new System.Drawing.Point(0, 0);
            this.propertyGrid_Indicator.Margin = new System.Windows.Forms.Padding(0);
            this.propertyGrid_Indicator.Name = "propertyGrid_Indicator";
            this.propertyGrid_Indicator.Size = new System.Drawing.Size(282, 225);
            this.propertyGrid_Indicator.TabIndex = 7;
            // 
            // IndicatorConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(282, 507);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(300, 1200);
            this.MinimizeBox = false;
            this.Name = "IndicatorConfig";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "⬙ Indicator Config";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.IndicatorConfig_FormClosing);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button button_SelectNoPattern;
        private System.Windows.Forms.Button button_SelectAllShort;
        private System.Windows.Forms.Button button_AllPatterns;
        private System.Windows.Forms.Button button_SelectAllLong;
        private System.Windows.Forms.CheckedListBox checkedListBox_Indicators;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button button_Hide;
        private System.Windows.Forms.PropertyGrid propertyGrid_Indicator;
    }
}