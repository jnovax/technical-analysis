﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CandleStickPattern
{
    public class ThreeLineStrikePattern : ChartIndicator
    {
        public override string Name => "Three Line Strike";

        public override string Result => SIGNAL_LONG;

        public override string GetSignal(IList<HLOC> data, int index)
        {
            if (index > 3)
            {
                var p1 = data[index - 3];
                var p2 = data[index - 2];
                var p3 = data[index - 1];
                var p4 = data[index];
                if (p1.Open > p1.Close &&
                 p3.Close < p2.Close && p2.Close < p1.Close &&
                 p4.Open < p3.Close &&
                 p4.Close > p1.Open)
                {
                    return Result;
                }
            }

            return NO_SIGNAL;
        }
    }
}
