﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms.DataVisualization.Charting;

namespace CandleStickPattern
{
    public abstract class ChartIndicator
    {
        public const string SIGNAL_LONG = "LONG";
        public const string SIGNAL_SHORT = "SHORT";
        public const string SIGNAL_BOTH = "BOTH";
        public const string NO_SIGNAL = "";

        public abstract string Name { get; }

        public abstract string Result { get; }

        public virtual string ShortName
        {
            get
            {
                var shortName = string.Concat(Name.Where(c => char.IsUpper(c)));
                if (shortName.Length < 3)
                {
                    shortName += Name[Name.Length - 1];
                }
                return shortName;
            }
        }

        public virtual bool Visible => true;

        public virtual string GetSignal(IList<HLOC> data, int index) => NO_SIGNAL;

        public virtual bool HasSeries => false;
        
        public virtual IList<double> GetSeriesData(IList<HLOC> data, int index) => null;

        public override string ToString()
        {
            switch (Result)
            {
                case SIGNAL_LONG:
                    return Name + " (\u25B2)";
                case SIGNAL_SHORT:
                    return Name + " (\u25BC)";
                case SIGNAL_BOTH:
                    return Name + " (\u25E9)";
                default:
                    return Name;
            }
        }
    }
}
