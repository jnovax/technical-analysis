﻿using System.Collections.Generic;
using System.Linq;

namespace CandleStickPattern.Indicators
{
    public class SMAIndicator : ChartIndicator
    {
        public int Period { get; set; } = 20;

        public override string Name => "Simple Moving Average";

        public override string Result => NO_SIGNAL;

        public override bool Visible => false;

        public override IList<double> GetSeriesData(IList<HLOC> data, int index)
        {
            var sma = double.NaN;
            if (index >= Period - 1)
            {
                sma = data.Skip(index - (Period - 1)).Take(Period).Average(x => x.Close);
            }

            return new[] { sma };
        }
    }
}
