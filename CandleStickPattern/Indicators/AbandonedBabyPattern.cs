﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CandleStickPattern
{
    public class AbandonedBabyPattern : ChartIndicator
    {
        public override string Name => "Abandoned Baby";

        public override string Result => SIGNAL_LONG;

        public override string GetSignal(IList<HLOC> data, int index)
        {

            if (index >= 20)
            {
                var ma10 = data.Skip(index - 10).Take(10).Average(x => x.Close);
                var ma20 = data.Skip(index - 20).Take(20).Average(x => x.Close);

                if (ma10 < ma20) // Short MA < Long MA => DOWN TREND
                {
                    var p1 = data[index - 2];
                    var p2 = data[index - 1];
                    var p3 = data[index];
                    if (p1.Open > p1.Close &&
                     p2.Open == p2.Close && 
                     p1.Close > p2.Open && 
                     p3.Close > p2.Close &&
                     p3.Open < p3.Close)
                    {
                        return Result;
                    }
                }
            }

            return NO_SIGNAL;
        }
    }
}
