﻿using System.Collections.Generic;
using System.Linq;

namespace CandleStickPattern
{
    public class BullishBeltHoldPattern : ChartIndicator
    {
        public override string Name => "Bullish Belt Hold";

        public override string Result => SIGNAL_LONG;

        public override string GetSignal(IList<HLOC> data, int index)
        {
            if (index >= 20)
            {
                var ma10 = data.Skip(index - 10).Take(10).Average(x => x.Close);
                var ma20 = data.Skip(index - 20).Take(20).Average(x => x.Close);

                if (ma10 < ma20) // Short MA < Long MA => DOWN TREND
                {
                    var p1 = data[index - 1];
                    var p2 = data[index];
                    if (p2.Close > p2.Open &&
                        p2.Open == p2.Low &&
                        p1.Open > p1.Close &&
                        p1.Close > p2.Open &&
                        p2.Close - p2.Open >= 0.3
                    )
                    {
                        return Result;
                    }
                }
            }

            return NO_SIGNAL;
        }
    }
}
