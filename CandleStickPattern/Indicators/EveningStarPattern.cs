﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CandleStickPattern
{
    public class EveningStarPattern : ChartIndicator
    {
        public override string Name => "Evening Star";

        public override string Result => SIGNAL_SHORT;

        public override string GetSignal(IList<HLOC> data, int index)
        {
            if (index >= 20)
            {
                var ma10 = data.Skip(index - 10).Take(10).Average(x => x.Close);
                var ma20 = data.Skip(index - 20).Take(20).Average(x => x.Close);

                if (ma10 > ma20) // Short MA > Long MA => UP TREND
                {
                    var p1 = data[index - 2];
                    var p2 = data[index - 1];
                    var p3 = data[index];

                    if (p1.Close > p1.Open && p1.Close - p1.Open >= 5 * Math.Abs(p2.Close - p2.Open) &&
                        //(p2.Close - p1.Close > 0.05 || p2.Open - p1.Close > 0.05) &&
                        p2.Close != p2.Open &&
                        //(p2.Close - p3.Close > 0.05 || p2.Open - p3.Close > 0.05) &&
                        p3.Open > p3.Close && p3.Open - p3.Close >= 3 * Math.Abs(p2.Close - p2.Open)
                        )
                    {
                        return Result;
                    }
                }
            }

            return NO_SIGNAL;
        }
    }
}
