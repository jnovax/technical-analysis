﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace CandleStickPattern.Indicators
{
    public class CommodityChannelIndex : ChartIndicator
    {
        protected double Factor = 0.015;
        private int m_period;

        [Category("Settings")]
        public int Period
        {
            get
            {
                return m_period;
            }
            set
            {
                m_period = value;
                SMA.Period = m_period;
            }
        }

        [Category("Settings")]
        public int LongThreshold { get; set; }

        [Category("Settings")]
        public int ShortThreshold { get; set; }

        [Category("Settings")]
        public bool FastReact { get; set; }

        protected readonly SMAIndicator SMA;
        public override string Name => "Commodity Channel Index";

        public override string Result => SIGNAL_BOTH;

        public CommodityChannelIndex() : base()
        {
            SMA = new SMAIndicator();
            Period = 60;
            LongThreshold = 100;
            ShortThreshold = -100;
            FastReact = false;
        }

        public override bool HasSeries => true;

        public override IList<double> GetSeriesData(IList<HLOC> data, int index)
        {
            var aveP = data.Select(x => new HLOC
            {
                Close = (x.High + x.Low + x.Close) / 3
            }).ToList();

            if (index >= Period - 1)
            {
                double smaValue = SMA.GetSeriesData(aveP, index)[0];
                double total = aveP.Skip(index - (Period - 1)).Take(Period).Select(x => Math.Abs(smaValue - x.Close)).Sum();
                double meanDeviation = total / (double)Period;
                double cci = (aveP[index].Close - smaValue) / (Factor * meanDeviation);

                return new[] { cci };
            }
            else
            {
                return new[] { double.NaN };
            }
        }

        public override string GetSignal(IList<HLOC> data, int index)
        {
            if (index > Period - 1)
            {
                var lastValue = GetSeriesData(data, index - 1)[0];
                var value = GetSeriesData(data, index)[0];

                if (FastReact) // Go on up-trend
                {
                    if (lastValue <= LongThreshold && value > LongThreshold) return SIGNAL_LONG;
                    if (lastValue >= ShortThreshold && value < ShortThreshold) return SIGNAL_SHORT;
                }
                else  // Go on down-trend
                {
                    if (lastValue >= LongThreshold && value < LongThreshold) return SIGNAL_LONG;
                    if (lastValue <= ShortThreshold && value > ShortThreshold) return SIGNAL_SHORT;
                }
            }
            return NO_SIGNAL;
        }
    }
}
