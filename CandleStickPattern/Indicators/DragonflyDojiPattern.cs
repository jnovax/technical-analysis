﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CandleStickPattern
{
    public class DragonflyDojiPattern : ChartIndicator
    {
        public override string Name => "Dragonfly Doji";

        public override string Result => SIGNAL_LONG;

        public override string GetSignal(IList<HLOC> data, int index)
        {
            if (index >= 20)
            {
                var ma10 = data.Skip(index - 10).Take(10).Average(x => x.Close);
                var ma20 = data.Skip(index - 20).Take(20).Average(x => x.Close);

                if (ma10 < ma20) // Short MA < Long MA => DOWN TREND
                {
                    var p = data[index];
                    if (p.Open == p.Close &&
                        p.Open - p.Low >= 0.3
                     )
                    {
                        return Result;
                    }
                }
            }

            return NO_SIGNAL;
        }
    }
}
