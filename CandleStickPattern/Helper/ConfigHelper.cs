﻿using Jil;
using System;
using System.Configuration;
using System.Linq;
using System.Windows.Forms;

namespace CandleStickPattern
{
    public static class ConfigHelper
    {
        public static DateTime EPOCH = new DateTime(1970, 1, 1, 0, 0, 0);
        private const string KEY_LAST_FILE = "LAST_FILE";

        private const string KEY_TRADE_CONFIG = "TRADE_CONFIG";

        private const string KEY_LAST_DIR = "LAST_DIR";
        private static readonly Configuration m_config;

        static ConfigHelper()
        {
            m_config = ConfigurationManager.OpenExeConfiguration(Application.ExecutablePath);
        }

        private static string GetConfig(string configKey)
        {
            if (m_config.AppSettings.Settings.AllKeys.Contains(configKey))
            {
                return m_config.AppSettings.Settings[configKey].Value;
            }
            return string.Empty;
        }

        private static void SaveConfig(string configKey, string configValue)
        {
            m_config.AppSettings.Settings.Remove(configKey);
            m_config.AppSettings.Settings.Add(configKey, configValue);

            m_config.Save(ConfigurationSaveMode.Modified);
        }

        public static string GetLastFile()
        {
            return GetConfig(KEY_LAST_FILE);
        }

        public static void SetLastFile(string fileName)
        {
            SaveConfig(KEY_LAST_FILE, fileName);
        }

        public static string GetLastDirectory()
        {
            return GetConfig(KEY_LAST_DIR);
        }

        public static void SetLastDirectory(string directory)
        {
            SaveConfig(KEY_LAST_DIR, directory);
        }

        public static void SetTradeConfig(TradeConfig tradeConfig)
        {
            SaveConfig(KEY_TRADE_CONFIG, JSON.Serialize(tradeConfig));
        }

        public static TradeConfig GetTradeConfig()
        {
            var config = GetConfig(KEY_TRADE_CONFIG);
            if (string.IsNullOrEmpty(config))
                return null;
            else
                return JSON.Deserialize<TradeConfig>(config);
        }

        public static DateTime GetEpochTime(long epochTime)
        {
            return EPOCH.AddMilliseconds(epochTime);
        }
    }
}
