﻿using Jil;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;


namespace CandleStickPattern
{
    public static class DataCacheHelper
    {
        private static readonly Dictionary<string, object> m_memCache = new Dictionary<string, object>();
        private static readonly string CACHE_PATH;
        private static bool m_useMemCache;

        static DataCacheHelper()
        {
            var ramCounter = new PerformanceCounter("Memory", "Available MBytes");
            if (ramCounter.NextValue() > 6 * 1024) // > 6Gb of RAM
            {
                m_useMemCache = true;
            }

            CACHE_PATH = Path.Combine(Application.StartupPath, "Cache");
            if (Directory.Exists(CACHE_PATH) == false)
                Directory.CreateDirectory(CACHE_PATH);
        }

        public static T LoadCache<T>(string key) where T : class
        {
            var cacheFile = Path.Combine(CACHE_PATH, key + ".cache");

            if (File.Exists(cacheFile))
            {
                T obj;

                if (m_memCache.ContainsKey(cacheFile))
                {
                    obj = (T)m_memCache[cacheFile];
                }
                else
                {
                    using (var reader = File.OpenText(cacheFile))
                    {
                        obj = JSON.Deserialize<T>(reader);
                    }

                    if (m_useMemCache && m_memCache.ContainsKey(cacheFile) == false)
                    {
                        m_memCache.Add(cacheFile, obj);
                    }
                }

                return obj;
            }
            return null;
        }

        public static void SaveCache<T>(string key, T obj) where T : class
        {
            var cacheFile = Path.Combine(CACHE_PATH, key + ".cache");

            if (File.Exists(cacheFile) == false)
            {
                using (var writer = File.CreateText(cacheFile))
                {
                    JSON.Serialize(obj, writer);
                }
                if (m_useMemCache && m_memCache.ContainsKey(cacheFile) == false)
                {
                    m_memCache.Add(cacheFile, obj);
                }
            }
        }
    }
}
