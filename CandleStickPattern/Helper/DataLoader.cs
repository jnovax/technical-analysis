﻿using Jil;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace CandleStickPattern
{
    public static class DataLoader
    {
        public static IEnumerable<OrderDealData> LoadKafkaFile(string filePath)
        {
            var fileName = Path.GetFileName(filePath);

            var sw = new Stopwatch();
            sw.Start();

            var orderDeals = DataCacheHelper.LoadCache<List<OrderDealData>>(fileName);
            if (orderDeals == null)
            {
                orderDeals = new List<OrderDealData>();
                foreach (string line in File.ReadLines(filePath))
                {
                    string[] lineData = line.Split('@');
                    var deal = JSON.Deserialize<OrderDeal>(lineData[1]).data.data;
                    deal.tick = Convert.ToInt64(lineData[0].Replace(",", "").Replace(".", ""));

                    orderDeals.Add(deal);
                }

                if (orderDeals.Count > 0)
                    DataCacheHelper.SaveCache(fileName, orderDeals);
            }

            sw.Stop();
            Console.WriteLine(sw.ElapsedMilliseconds);

            return orderDeals;
        }
    }
}
