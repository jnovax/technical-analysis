﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace CandleStickPattern
{
    public partial class FormMain : Form
    {
        private static int TIME_RANGE = 60000;
        internal Dictionary<ChartIndicator, bool> Indicators = new Dictionary<ChartIndicator, bool>();

        private string m_fileName;
        private IEnumerable<string> m_selectedFolders;
        private TradeConfig m_tradeConfig;
        private List<HLOC> m_chartData = new List<HLOC>();

        internal bool IndicatorLoad = false;

        public static FormMain TheMainWnd;
        private IndicatorConfig m_indicatorConfig;

        public FormMain()
        {
            InitializeComponent();

            var chartSeries = chart_CandleStick.Series["DealsHLOC"];
            chartSeries.LabelForeColor = Color.White;

            var chartArea = chart_CandleStick.ChartAreas["PriceChartArea"];
            chartArea.AxisY.ScaleView.Zoomable = true;
            chartArea.CursorY.Interval = 0.1;

            chartArea.AxisX.ScaleView.Zoomable = true;
            chartArea.CursorX.Interval = 0.1;

            var indicatorTypes = Assembly.GetExecutingAssembly().GetTypes().Where(x => typeof(ChartIndicator).IsAssignableFrom(x) && x.IsAbstract == false).ToList();

            foreach (var type in indicatorTypes)
            {
                var indicator = (ChartIndicator)Activator.CreateInstance(type);

                if (indicator.Visible)
                {
                    if (indicator.HasSeries)
                    {
                        var series = new Series(indicator.ShortName)
                        {
                            ChartType = SeriesChartType.FastLine,
                            Color = Color.DimGray,
                            ChartArea = chartArea.Name,
                            YAxisType = AxisType.Secondary
                        };
                        chart_CandleStick.Series.Add(series);
                    }

                    Indicators.Add(indicator, false);
                }
            }

            Indicators = Indicators.OrderBy(x => x.Key.Result).ToDictionary(x => x.Key, x => x.Value);
            IndicatorLoad = true;

            numericUpDown_TimeFrame.Value = TIME_RANGE / 1000;
            TheMainWnd = this;
            button_ShowHideLog.Left = 0;
            button_ShowHideLog.Top = splitContainer1.Panel1.Height - button_ShowHideLog.Height;
            button_ToggleFullscreen.Top = 0;
            button_ToggleFullscreen.Left = splitContainer1.Panel1.Width - button_ToggleFullscreen.Width;
        }

        private void Button_ImportData_Click(object sender, EventArgs e)
        {
            var fileDialog = new OpenFileDialog
            {
                Filter = "Kafka Files|*.kafkalog",
            };

            var initDirectory = ConfigHelper.GetLastFile();
            if (string.IsNullOrEmpty(initDirectory))
            {
                initDirectory = Application.ExecutablePath;
            }
            fileDialog.InitialDirectory = Path.GetDirectoryName(initDirectory);

            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                splitContainer1.Panel1Collapsed = false;
                splitContainer1.Panel2Collapsed = true;

                ClearLog();
                m_fileName = fileDialog.FileName;
                ConfigHelper.SetLastFile(m_fileName);

                backgroundWorkerImportOneDay.RunWorkerAsync();

                panel_ControlBar.Enabled = false;
            }
        }

        public void ClearLog()
        {
            if (InvokeRequired)
            {
                Invoke((MethodInvoker)delegate
                {
                    ClearLog();
                });
            }
            else
            {
                richTextBox_Log.Clear();
            }
        }

        public void ShowLog(string text, Color textColor, bool bold = false)
        {
            if (InvokeRequired)
            {
                Invoke((MethodInvoker)delegate
                {
                    ShowLog(text, textColor);
                });
            }
            else
            {
                richTextBox_Log.SelectionStart = richTextBox_Log.TextLength;
                richTextBox_Log.SelectionLength = 0;
                richTextBox_Log.SelectionColor = textColor;
                if (bold)
                {
                    richTextBox_Log.SelectionFont = new Font(richTextBox_Log.Font, FontStyle.Bold);
                }
                richTextBox_Log.AppendText(DateTime.Now.ToShortTimeString() + ": " + text + Environment.NewLine);
                richTextBox_Log.SelectionColor = richTextBox_Log.ForeColor;
                richTextBox_Log.ScrollToCaret();
            };
        }

        internal void ResetIndicatorDrawing()
        {
            if (m_chartData.Count > 0)
            {
                chart_CandleStick.SuspendLayout();
                var chartSeries = chart_CandleStick.Series["DealsHLOC"];
                for (var i = 0; i < m_chartData.Count; i++)
                {
                    chartSeries.Points[i].MarkerStyle = MarkerStyle.None;
                    chartSeries.Points[i].Label = string.Empty;
                }

                // Clear old data
                foreach (var indicator in Indicators.Keys.Where(x => x.HasSeries))
                {
                    chart_CandleStick.Series[indicator.ShortName].Points.Clear();
                }

                var selectIndicators = Indicators.Where(x => x.Value).Select(x => x.Key);

                foreach (var indicator in selectIndicators)
                {
                    for (var i = 0; i < m_chartData.Count; i++)
                    {
                        if (indicator.HasSeries)
                        {
                            var seriesData = indicator.GetSeriesData(m_chartData, i);
                            if (double.IsNaN(seriesData[0]) == false)
                            {
                                chart_CandleStick.Series[indicator.ShortName].Points.AddXY(i, seriesData[0]);
                            }
                        }

                        var signal = indicator.GetSignal(m_chartData, i);
                        if (signal != ChartIndicator.NO_SIGNAL)
                        {
                            chartSeries.Points[i].Label = indicator.ShortName;

                            if (signal == ChartIndicator.SIGNAL_LONG)
                            {
                                chartSeries.Points[i].MarkerStyle = MarkerStyle.Triangle;
                                chartSeries.Points[i].MarkerColor = Color.Lime;
                                ShowLog($"{indicator.Name}: Got signal {signal} at {i}", Color.Lime);
                            }

                            if (signal == ChartIndicator.SIGNAL_SHORT)
                            {
                                chartSeries.Points[i].MarkerStyle = MarkerStyle.Square;
                                chartSeries.Points[i].MarkerColor = Color.Red;
                                ShowLog($"{indicator.Name}: Got signal {signal} at {i}", Color.Red);
                            }
                        }
                    }
                }
                chart_CandleStick.ResumeLayout();
            }
        }

        private void BackgroundWorkerImportOneDay_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            panel_ControlBar.Enabled = true;
            ShowLog("DONE", Color.LimeGreen);
        }

        private void BackgroundWorkerImportOneDay_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                double min = 1000000;
                double max = 0;
                m_chartData = new List<HLOC>();
                var fromTick = -1L;
                var toTick = -1L;

                var matchOrders = new List<OrderDealData>();

                ShowLog($"Start importing data from {Path.GetFileName(m_fileName)}", Color.White);

                foreach (var order in DataLoader.LoadKafkaFile(m_fileName))
                {
                    if (order.msg_type == "2E" && order.oType == "N")
                    {
                        if (fromTick == -1)
                        {
                            fromTick = order.tick;
                            toTick = order.tick + TIME_RANGE;
                        }

                        if (order.tick > toTick)
                        {
                            var point = new HLOC
                            {
                                Time = ConfigHelper.GetEpochTime(order.tick),
                                High = -1,
                                Low = 100000000,
                                Open = 0,
                                Close = 0
                            };

                            point.Open = (double)matchOrders[0].matchValue / (double)matchOrders[0].matchVolume / 100;
                            point.Close = (double)matchOrders[matchOrders.Count - 1].matchValue / (double)matchOrders[matchOrders.Count - 1].matchVolume / 100;
                            foreach (var d in matchOrders)
                            {
                                double matchedPrice = (double)d.matchValue / (double)d.matchVolume / 100;
                                if (matchedPrice > point.High) point.High = matchedPrice;
                                if (matchedPrice < point.Low) point.Low = matchedPrice;
                            }

                            if (min > point.Close) min = point.Close;
                            if (max < point.Close) max = point.Close;

                            m_chartData.Add(point);

                            fromTick = toTick;
                            toTick = order.tick + TIME_RANGE;
                            matchOrders.Clear();
                        }
                        matchOrders.Add(order);
                    }
                }

                ShowLog($"Got {m_chartData.Count} bars, drawing to chart", Color.White);

                if (m_chartData.Count > 0)
                {
                    Invoke((MethodInvoker)delegate
                    {
                        foreach (var series in chart_CandleStick.Series)
                        {
                            series.Points.Clear();
                        }

                        var chartSeries = chart_CandleStick.Series["DealsHLOC"];
                        var chartArea = chart_CandleStick.ChartAreas["PriceChartArea"];

                        for (var i = 0; i < m_chartData.Count; i++)
                        {
                            chartSeries.Points.AddXY(i, m_chartData[i].GetValues());
                        }

                        ResetIndicatorDrawing();

                        chartArea.AxisY.Minimum = Math.Round((double)min - 0.1, 1);
                        chartArea.AxisY.Maximum = Math.Round((double)max + 0.1, 1);
                    });
                }
            }
            catch (Exception ex)
            {
                ShowLog($"ERROR: {ex}", Color.Red);
            }
        }

        private void Button_ScanDataFolder_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel1Collapsed = true;
            splitContainer1.Panel2Collapsed = false;

            var dataScanConfig = new DataScanConfig();
            if (dataScanConfig.ShowDialog() == DialogResult.OK)
            {
                ClearLog();
                m_chartData.Clear();
                m_selectedFolders = dataScanConfig.Folders;
                m_tradeConfig = dataScanConfig.TradeConfig;

                ConfigHelper.SetLastDirectory(string.Join(",", m_selectedFolders));

                backgroundWorkerScanFolder.RunWorkerAsync();

                panel_ControlBar.Enabled = false;
            }
        }

        private void BackgroundWorkerScanFolder_DoWork(object sender, DoWorkEventArgs e)
        {
            var logFiles = m_selectedFolders.SelectMany(x =>
                Directory.EnumerateFiles(x, "*.kafkalog", SearchOption.AllDirectories)
            );
            var totalProfit = 0d;

            foreach (var fileName in logFiles)
            {
                try
                {
                    var chartData = new List<HLOC>();
                    var fromTick = -1L;
                    var toTick = -1L;
                    var candleOrders = new List<OrderDealData>();
                    var stats = new Dictionary<string, List<int>>();

                    ShowLog($"Start importing data from {Path.GetFileName(fileName)}", Color.White);
                    foreach (var order in DataLoader.LoadKafkaFile(fileName))
                    {
                        if (order.msg_type == "2E" && order.oType == "N")
                        {
                            if (fromTick == -1)
                            {
                                fromTick = order.tick;
                                toTick = order.tick + TIME_RANGE;
                            }

                            if (order.tick > toTick)
                            {
                                var point = new HLOC
                                {
                                    Time = ConfigHelper.GetEpochTime(order.tick),
                                    High = -1,
                                    Low = 100000000,
                                    Open = 0,
                                    Close = 0
                                };

                                point.Open = (double)candleOrders[0].matchValue / (double)candleOrders[0].matchVolume / 100;
                                point.Close = (double)candleOrders[candleOrders.Count - 1].matchValue / (double)candleOrders[candleOrders.Count - 1].matchVolume / 100;
                                foreach (var d in candleOrders)
                                {
                                    double matchedPrice = (double)d.matchValue / (double)d.matchVolume / 100;
                                    if (matchedPrice > point.High) point.High = matchedPrice;
                                    if (matchedPrice < point.Low) point.Low = matchedPrice;
                                }

                                chartData.Add(point);

                                fromTick = toTick;
                                toTick = order.tick + TIME_RANGE;
                                candleOrders.Clear();
                            }
                            candleOrders.Add(order);
                        }
                    }

                    ShowLog($"Got {chartData.Count} bars", Color.White);

                    if (chartData.Count > 0)
                    {
                        for (var i = 0; i < chartData.Count; i++)
                        {
                            foreach (var chartPattern in Indicators.Where(x => x.Value).Select(x => x.Key))
                            {
                                var signal = chartPattern.GetSignal(chartData, i);
                                if (signal != ChartIndicator.NO_SIGNAL)
                                {
                                    var name = "\u2705 " + chartPattern.Name + "." + signal;
                                    if (stats.ContainsKey(name) == false)
                                    {
                                        stats[name] = new List<int>();
                                    }
                                    stats[name].Add(i);
                                }
                            }

                            foreach (var chartPattern in Indicators.Where(x => x.Value == false).Select(x => x.Key))
                            {
                                var signal = chartPattern.GetSignal(chartData, i);
                                if (signal != ChartIndicator.NO_SIGNAL)
                                {
                                    var name = chartPattern.Name + "." + signal;
                                    if (stats.ContainsKey(name) == false)
                                    {
                                        stats[name] = new List<int>();
                                    }
                                    stats[name].Add(i);
                                }
                            }
                        }

                        foreach (var statItem in stats.Where(x => x.Value.Count > 0))
                        {
                            var data = statItem.Key.Split('.');
                            var color = Color.RosyBrown;

                            if (data[1] == ChartIndicator.SIGNAL_LONG)
                            {
                                color = Color.DarkGreen;
                            }

                            ShowLog($"{data[0]}: Got {statItem.Value.Count} {data[1]} signal(s): [{string.Join(", ", statItem.Value)}]", color);
                        }

                        var profit = GetProfit(chartData, m_tradeConfig);
                        ShowLog($"Profit/Loss = {profit.ToString("N2")}", (profit > 0 ? Color.Green : Color.DarkRed), true);
                        totalProfit += profit;
                    }
                }
                catch (Exception ex)
                {
                    ShowLog($"ERROR: {ex}", Color.Red);
                }
            }

            if (totalProfit > 0)
            {
                ShowLog($"Profit = {totalProfit.ToString("N2")}", Color.LimeGreen, true);
            }
            else
            {
                ShowLog($"Loss = {totalProfit.ToString("N2")}", Color.Red, true);
            }
        }

        private void BackgroundWorkerScanFolder_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            panel_ControlBar.Enabled = true;
            ShowLog("DONE", Color.LimeGreen);
            MessageBox.Show("DONE!");
        }

        private void NumericUpDown_TimeFrame_ValueChanged(object sender, EventArgs e)
        {
            TIME_RANGE = (int)numericUpDown_TimeFrame.Value * 1000;
        }

        private void Chart_CandleStick_MouseMove(object sender, MouseEventArgs e)
        {
            var chartArea = chart_CandleStick.ChartAreas[0];
            var chartRect = InnerPlotPositionClientRectangle(chart_CandleStick, chartArea);

            if (chartRect.Contains(e.Location) && m_chartData.Count > 0)
            {
                chartArea.CursorX.SetCursorPixelPosition(e.Location, true);
                chartArea.CursorY.SetCursorPixelPosition(e.Location, true);

                label_X_Axis.Text = chartArea.AxisX.PixelPositionToValue(e.X).ToString("N0");
                label_Y_Axis.Text = chartArea.AxisY.PixelPositionToValue(e.Y).ToString("N2");

                label_X_Axis.Location = new Point(e.X + 1, (int)chartRect.Top);
                label_Y_Axis.Location = new Point((int)chartRect.Right - label_Y_Axis.Width - 1, e.Y - label_Y_Axis.Height);

                label_X_Axis.Visible = true;
                label_Y_Axis.Visible = true;
            }
            else
            {
                label_X_Axis.Visible = false;
                label_Y_Axis.Visible = false;
            }
        }

        RectangleF GetChartAreaClientRectangle(Chart chart, ChartArea chartArea)
        {
            RectangleF CAR = chartArea.Position.ToRectangleF();
            float pw = chart.ClientSize.Width / 100f;
            float ph = chart.ClientSize.Height / 100f;
            return new RectangleF(pw * CAR.X, ph * CAR.Y, pw * CAR.Width, ph * CAR.Height);
        }

        RectangleF InnerPlotPositionClientRectangle(Chart chart, ChartArea chartArea)
        {
            RectangleF areaInnerPos = chartArea.InnerPlotPosition.ToRectangleF();
            RectangleF CArp = GetChartAreaClientRectangle(chart, chartArea);

            float pw = CArp.Width / 100f;
            float ph = CArp.Height / 100f;

            return new RectangleF(CArp.X + pw * areaInnerPos.X, CArp.Y + ph * areaInnerPos.Y,
                                    pw * areaInnerPos.Width, ph * areaInnerPos.Height);
        }

        private void Button_ShowHideLog_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel2Collapsed = !splitContainer1.Panel2Collapsed;

            if (splitContainer1.Panel2Collapsed)
            {
                button_ShowHideLog.Text = "\u25E5";
                button_ShowHideLog.Tag = "HIDE";
            }
            else
            {
                button_ShowHideLog.Text = "\u25E3";
                button_ShowHideLog.Tag = "SHOW";
            }
        }

        private void Button_IndicatorConfig_Click(object sender, EventArgs e)
        {
            if (m_indicatorConfig == null)
            {
                m_indicatorConfig = new IndicatorConfig();
                m_indicatorConfig.Left = MousePosition.X - m_indicatorConfig.Width + 50;
                m_indicatorConfig.Top = MousePosition.Y;
                m_indicatorConfig.Show(this);
            }
            else
            {
                if (m_indicatorConfig.Visible == false)
                {
                    m_indicatorConfig.Left = MousePosition.X - m_indicatorConfig.Width + 50;
                    m_indicatorConfig.Top = MousePosition.Y - 50;
                    m_indicatorConfig.Visible = true;
                }
                else
                {
                    m_indicatorConfig.Hide();
                }
            }
        }

        private void Button_ToggleFullscreen_Click(object sender, EventArgs e)
        {
            if (panel_ControlBar.Height == 0) // Go NORMAL
            {
                panel_ControlBar.Height = 57;
                splitContainer1.Panel2Collapsed = !button_ShowHideLog.Tag.Equals("SHOW");

                FormBorderStyle = FormBorderStyle.Sizable;
                WindowState = FormWindowState.Normal;
            }
            else // Go FULLSCREEN
            {
                panel_ControlBar.Height = 0;
                splitContainer1.Panel2Collapsed = true;

                WindowState = FormWindowState.Normal;
                FormBorderStyle = FormBorderStyle.None;
                WindowState = FormWindowState.Maximized;
            }
        }

        internal void AutoTrade(TradeConfig config)
        {
            if (m_chartData.Count > 0)
            {
                if (splitContainer1.Panel2Collapsed)
                {
                    Button_ShowHideLog_Click(this, new EventArgs());
                }

                ClearLog();
                var orders = new List<Order>();

                for (var i = 0; i < m_chartData.Count; i++)
                {
                    var pendingOrders = orders.Where(x => x.IsDone == false).ToList();

                    foreach (var order in pendingOrders)
                    {
                        var action = order.PerformStep(m_chartData[i].Close);
                        if (string.IsNullOrEmpty(action) == false)
                        {
                            ShowLog($"Order[{order.Id}] {order.Position}@{order.Price.ToString("N2")} has {action} at {m_chartData[i].Close.ToString("N2")}@[{i}-{m_chartData[i].Time.ToString("HH:mm:ss")}]", Color.LimeGreen);
                        }
                    }

                    foreach (var chartPattern in Indicators.Where(x => x.Value).Select(x => x.Key))
                    {
                        var signal = chartPattern.GetSignal(m_chartData, i);
                        if (signal != ChartIndicator.NO_SIGNAL)
                        {
                            if (config.SingleContract)
                            {
                                var pendingOrder = orders.Where(x => x.IsDone == false).SingleOrDefault();
                                if (pendingOrder != null)
                                {
                                    if (pendingOrder.Position == signal)
                                    {
                                        pendingOrder.ResetGreed(m_chartData[i].Close);
                                        continue;
                                    }
                                    else
                                    {
                                        pendingOrder.Settle(m_chartData[i].Close);
                                        ShowLog($"Order[{pendingOrder.Id}] {pendingOrder.Position}@{pendingOrder.Price.ToString("N2")} has SETTLED at {m_chartData[i].Close.ToString("N2")}@[{i}-{m_chartData[i].Time.ToString("HH:mm:ss")}]", Color.LimeGreen);
                                    }
                                }
                            }

                            var order = new Order(i, m_chartData[i].Close, config.Threshold, signal, config.GreedMode);
                            orders.Add(order);
                            ShowLog($"Order[{order.Id}] {order.Position}@{order.Price.ToString("N2")} has PLACE at {m_chartData[i].Close.ToString("N2")}@[{m_chartData[i].Time.ToString("HH:mm:ss")}]", Color.LimeGreen);
                        }
                    }
                }

                var remainOrders = orders.Where(x => x.IsDone == false).ToList();
                foreach (var order in remainOrders)
                {
                    var i = m_chartData.Count - 1;
                    var action = order.Settle(m_chartData[i].Close);
                    ShowLog($"Order[{order.Id}] {order.Position}@{order.Price.ToString("N2")} has {action} at {m_chartData[i].Close.ToString("N2")}@[{i}-{m_chartData[i].Time.ToString("HH:mm:ss")}]", Color.LimeGreen);
                }

                ShowLog($"Profit/Loss = {orders.Select(x => x.Profit(config.Fee)).Sum().ToString("N2")}", Color.BlueViolet, true);
            }
            else
            {
                ShowLog("No data!", Color.Red);
            }
        }

        private double GetProfit(List<HLOC> chartData, TradeConfig tradeConfig)
        {
            var orders = new List<Order>();

            for (var i = 0; i < chartData.Count; i++)
            {
                var pendingOrders = orders.Where(x => x.IsDone == false).ToList();

                foreach (var order in pendingOrders)
                {
                    order.PerformStep(chartData[i].Close);
                }

                foreach (var chartPattern in Indicators.Where(x => x.Value).Select(x => x.Key))
                {
                    var signal = chartPattern.GetSignal(chartData, i);
                    if (signal != ChartIndicator.NO_SIGNAL)
                    {
                        if (tradeConfig.SingleContract)
                        {
                            var pendingOrder = orders.Where(x => x.IsDone == false).SingleOrDefault();
                            if (pendingOrder != null)
                            {
                                if (pendingOrder.Position == signal)
                                {
                                    pendingOrder.ResetGreed(chartData[i].Close);
                                    continue;
                                }
                                else
                                {
                                    pendingOrder.Settle(chartData[i].Close);
                                }
                            }
                        }

                        orders.Add(new Order(i, chartData[i].Close, tradeConfig.Threshold, signal, tradeConfig.GreedMode));
                    }
                }
            }

            var remainOrders = orders.Where(x => x.IsDone == false).ToList();
            foreach (var order in remainOrders)
            {
                order.Settle(chartData[chartData.Count - 1].Close);
            }

            return orders.Select(x => x.Profit(tradeConfig.Fee)).Sum();
        }

        private void Button_AutoTrade_Click(object sender, EventArgs e)
        {
            var autoTrade = new AutoTradeConfig();
            autoTrade.Show(this);
        }
    }
}
