﻿using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace CandleStickPattern
{
    public partial class IndicatorConfig : Form
    {
        public IndicatorConfig()
        {
            InitializeComponent();
            ResetPatternList();

            splitContainer1.Panel2Collapsed = true;
        }

        private void ResetPatternList()
        {
            checkedListBox_Indicators.SuspendLayout();
            checkedListBox_Indicators.Items.Clear();
            foreach (var pattern in FormMain.TheMainWnd.Indicators)
            {
                checkedListBox_Indicators.Items.Add(pattern.Key, pattern.Value);
            }
            checkedListBox_Indicators.ResumeLayout();
        }

        private void Button_SelectAllShort_Click(object sender, EventArgs e)
        {
            FormMain.TheMainWnd.IndicatorLoad = false;

            var patterns = FormMain.TheMainWnd.Indicators.Keys.ToArray();
            foreach (var pattern in patterns)
            {
                FormMain.TheMainWnd.Indicators[pattern] = (pattern.Result == ChartIndicator.SIGNAL_SHORT || pattern.Result == ChartIndicator.SIGNAL_BOTH);
            }

            ResetPatternList();
            FormMain.TheMainWnd.IndicatorLoad = true;
            CheckedListBox_Patterns_ItemCheck(this, null);
        }

        private void Button_AllPatterns_Click(object sender, EventArgs e)
        {
            FormMain.TheMainWnd.IndicatorLoad = false;

            var patterns = FormMain.TheMainWnd.Indicators.Keys.ToArray();
            foreach (var pattern in patterns)
            {
                FormMain.TheMainWnd.Indicators[pattern] = true;
            }

            ResetPatternList();
            FormMain.TheMainWnd.IndicatorLoad = true;
            CheckedListBox_Patterns_ItemCheck(this, null);
        }

        private void Button_SelectAllLong_Click(object sender, EventArgs e)
        {
            FormMain.TheMainWnd.IndicatorLoad = false;

            var patterns = FormMain.TheMainWnd.Indicators.Keys.ToArray();
            foreach (var pattern in patterns)
            {
                FormMain.TheMainWnd.Indicators[pattern] = (pattern.Result == ChartIndicator.SIGNAL_LONG || pattern.Result == ChartIndicator.SIGNAL_BOTH);
            }

            ResetPatternList();
            FormMain.TheMainWnd.IndicatorLoad = true;
            CheckedListBox_Patterns_ItemCheck(this, null);
        }

        private void Button_SelectNoPattern_Click(object sender, EventArgs e)
        {
            FormMain.TheMainWnd.IndicatorLoad = false;

            var patterns = FormMain.TheMainWnd.Indicators.Keys.ToArray();
            foreach (var pattern in patterns)
            {
                FormMain.TheMainWnd.Indicators[pattern] = false;
            }

            ResetPatternList();
            FormMain.TheMainWnd.IndicatorLoad = true;
            CheckedListBox_Patterns_ItemCheck(this, null);
        }

        private void CheckedListBox_Patterns_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            if (!FormMain.TheMainWnd.IndicatorLoad)
                return;

            if (e != null)
            {
                var pattern = checkedListBox_Indicators.Items[e.Index] as ChartIndicator;
                FormMain.TheMainWnd.Indicators[pattern] = (e.NewValue == CheckState.Checked);
            }

            FormMain.TheMainWnd.ResetIndicatorDrawing();
        }

        private void IndicatorConfig_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                Hide();
                e.Cancel = true;
            }
        }

        private void Button_Hide_Click(object sender, EventArgs e)
        {
            splitContainer1.Panel2Collapsed = true;
        }

        private void CheckedListBox_Indicators_MouseClick(object sender, MouseEventArgs e)
        {
            
        }

        private void CheckedListBox_Indicators_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (checkedListBox_Indicators.SelectedIndex >= 0)
                {
                    var pattern = checkedListBox_Indicators.Items[checkedListBox_Indicators.SelectedIndex] as ChartIndicator;
                    propertyGrid_Indicator.SelectedObject = pattern;

                    splitContainer1.Panel2Collapsed = false;
                }
            }
        }
    }
}
