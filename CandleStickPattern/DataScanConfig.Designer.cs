﻿namespace CandleStickPattern
{
    partial class DataScanConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_RootFolder = new System.Windows.Forms.TextBox();
            this.button_Browse = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.checkBox_GreedMode = new System.Windows.Forms.CheckBox();
            this.numericUpDown_ProfitThreshold = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_Fee = new System.Windows.Forms.NumericUpDown();
            this.button_Scan = new System.Windows.Forms.Button();
            this.checkBox_SingleContract = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ProfitThreshold)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Fee)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Data Folders";
            // 
            // textBox_RootFolder
            // 
            this.textBox_RootFolder.Location = new System.Drawing.Point(13, 49);
            this.textBox_RootFolder.Multiline = true;
            this.textBox_RootFolder.Name = "textBox_RootFolder";
            this.textBox_RootFolder.ReadOnly = true;
            this.textBox_RootFolder.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox_RootFolder.Size = new System.Drawing.Size(423, 98);
            this.textBox_RootFolder.TabIndex = 2;
            // 
            // button_Browse
            // 
            this.button_Browse.Location = new System.Drawing.Point(361, 13);
            this.button_Browse.Name = "button_Browse";
            this.button_Browse.Size = new System.Drawing.Size(75, 23);
            this.button_Browse.TabIndex = 3;
            this.button_Browse.Text = "Browse";
            this.button_Browse.UseVisualStyleBackColor = true;
            this.button_Browse.Click += new System.EventHandler(this.Button_Browse_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(238, 179);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(198, 17);
            this.label2.TabIndex = 11;
            this.label2.Text = "Stop Loss is 2/3 of Take Profit";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 179);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 17);
            this.label3.TabIndex = 8;
            this.label3.Text = "Take Profit";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(60, 221);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 17);
            this.label4.TabIndex = 9;
            this.label4.Text = "Fee";
            // 
            // checkBox_GreedMode
            // 
            this.checkBox_GreedMode.AutoSize = true;
            this.checkBox_GreedMode.Checked = true;
            this.checkBox_GreedMode.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_GreedMode.Location = new System.Drawing.Point(241, 220);
            this.checkBox_GreedMode.Name = "checkBox_GreedMode";
            this.checkBox_GreedMode.Size = new System.Drawing.Size(109, 21);
            this.checkBox_GreedMode.TabIndex = 10;
            this.checkBox_GreedMode.Text = "Greed Mode";
            this.checkBox_GreedMode.UseVisualStyleBackColor = true;
            // 
            // numericUpDown_ProfitThreshold
            // 
            this.numericUpDown_ProfitThreshold.DecimalPlaces = 1;
            this.numericUpDown_ProfitThreshold.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDown_ProfitThreshold.Location = new System.Drawing.Point(98, 177);
            this.numericUpDown_ProfitThreshold.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDown_ProfitThreshold.Name = "numericUpDown_ProfitThreshold";
            this.numericUpDown_ProfitThreshold.Size = new System.Drawing.Size(108, 22);
            this.numericUpDown_ProfitThreshold.TabIndex = 6;
            this.numericUpDown_ProfitThreshold.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numericUpDown_ProfitThreshold.Value = new decimal(new int[] {
            15,
            0,
            0,
            65536});
            // 
            // numericUpDown_Fee
            // 
            this.numericUpDown_Fee.Increment = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDown_Fee.Location = new System.Drawing.Point(98, 219);
            this.numericUpDown_Fee.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDown_Fee.Name = "numericUpDown_Fee";
            this.numericUpDown_Fee.Size = new System.Drawing.Size(108, 22);
            this.numericUpDown_Fee.TabIndex = 7;
            this.numericUpDown_Fee.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numericUpDown_Fee.ThousandsSeparator = true;
            this.numericUpDown_Fee.Value = new decimal(new int[] {
            9000,
            0,
            0,
            0});
            // 
            // button_Scan
            // 
            this.button_Scan.Location = new System.Drawing.Point(158, 304);
            this.button_Scan.Name = "button_Scan";
            this.button_Scan.Size = new System.Drawing.Size(132, 45);
            this.button_Scan.TabIndex = 12;
            this.button_Scan.Text = "&Scan";
            this.button_Scan.UseVisualStyleBackColor = true;
            this.button_Scan.Click += new System.EventHandler(this.Button_Scan_Click);
            // 
            // checkBox_SingleContract
            // 
            this.checkBox_SingleContract.AutoSize = true;
            this.checkBox_SingleContract.Location = new System.Drawing.Point(241, 259);
            this.checkBox_SingleContract.Name = "checkBox_SingleContract";
            this.checkBox_SingleContract.Size = new System.Drawing.Size(165, 21);
            this.checkBox_SingleContract.TabIndex = 13;
            this.checkBox_SingleContract.Text = "Single Contract Mode";
            this.checkBox_SingleContract.UseVisualStyleBackColor = true;
            // 
            // DataScanConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(452, 365);
            this.Controls.Add(this.checkBox_SingleContract);
            this.Controls.Add(this.button_Scan);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.checkBox_GreedMode);
            this.Controls.Add(this.numericUpDown_ProfitThreshold);
            this.Controls.Add(this.numericUpDown_Fee);
            this.Controls.Add(this.button_Browse);
            this.Controls.Add(this.textBox_RootFolder);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "DataScanConfig";
            this.Text = "Data Scan Config";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_ProfitThreshold)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Fee)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_RootFolder;
        private System.Windows.Forms.Button button_Browse;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox checkBox_GreedMode;
        private System.Windows.Forms.NumericUpDown numericUpDown_ProfitThreshold;
        private System.Windows.Forms.NumericUpDown numericUpDown_Fee;
        private System.Windows.Forms.Button button_Scan;
        private System.Windows.Forms.CheckBox checkBox_SingleContract;
    }
}